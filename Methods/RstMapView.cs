﻿
using Bearing;
using GrozaSModelsDBLib;
using Mapsui.Projection;
using Mapsui.Providers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using UIMap.Events;
using UIMap.Models;
using WpfMapControl;


namespace UIMap
{
    public partial class RstMapView : UserControl, INotifyPropertyChanged
    {
       
        public event EventHandler<Location> OnJammerPosition;
        public event EventHandler<Location> OnSpoofingPosition;
        public event EventHandler<Location> OnZorkiRPosition;
        public event EventHandler<float> OnZorkiRDirection;
        public event EventHandler<Location> OnGlobusPosition;
        public event EventHandler<Location> OnSetTargetForGrozaR;

        public event EventHandler<float> OnDirection;

        public event EventHandler<float> OnDirectionJamming;
        public event EventHandler<float> OnDirectionJammingSecond;
        public event EventHandler<float> OnDirectionConnection;
        public event EventHandler<float> OnDirectionConnectionSecond;

     
        private VDrone vSpoofingTrackObject = new VDrone();
        private List<VJammer> vJammerObject = new List<VJammer>();
        private List<VDrone> vDroneObject = new List<VDrone>();
        private List<VDrone> vRodnikObject = new List<VDrone>();
        private List<VDrone> vZorkiRObject = new List<VDrone>();
        private List<VDrone> vGlobusObject = new List<VDrone>();

        private List<VDrone> vAeroscopeObject = new List<VDrone>();
        private List<VMStaticObject> vAeroscopeHomeObject = new List<VMStaticObject>();
        
        private List<VDrone> vEscopeObject = new List<VDrone>();
        private List<VMStaticObject> vEscopeHomeObject = new List<VMStaticObject>();
        private List<VMStaticObject> vEscopePilotObject = new List<VMStaticObject>();

        private List<VDrone> vCuirasseObject = new List<VDrone>();
        private List<VMStaticObject> vCuirassePoints = new List<VMStaticObject>();


        private List<VGrozaR> vGrozaRObject = new List<VGrozaR>();

        private Feature _featureRout;

        private IMapObject _objectSpoofing;
        private IMapObject _objectGNSS;
        private IMapObject _objectStartPosition;
        private IMapObject _objectStopPosition;



        private MapObjectStyle _spoofingObjectStyle;
        private MapObjectStyle _spoofingTrackObjectStyle;
        private MapObjectStyle _gnssObjectStyle;
        private MapObjectStyle _jammerObjectStyle;
        private MapObjectStyle _multicopterObjectStyle;
        private MapObjectStyle _unknownObjectStyle;
        private MapObjectStyle _gsmObjectStyle;
        private MapObjectStyle _planeObjectStyle;
        private MapObjectStyle _startPointObjectStyle;
        private MapObjectStyle _stopPointObjectStyle;
        private MapObjectStyle _grozaRObjectStyle;


        private MapObjectStyle _ballisticRodnikObjectStyle;
        private MapObjectStyle _balloonRodnikObjectStyle;
        private MapObjectStyle _helicopterRodnikObjectStyle;
        private MapObjectStyle _planeRodnikObjectStyle;
        private MapObjectStyle _unknownRodnikObjectStyle;


        private MapObjectStyle _humanZorkiRObjectStyle;
        private MapObjectStyle _carZorkiRObjectStyle;
        private MapObjectStyle _helicopterZorkiRObjectStyle;
        private MapObjectStyle _groupZorkiRObjectStyle;
        private MapObjectStyle _UAVZorkiRObjectStyle;
        private MapObjectStyle _IFVZorkiRObjectStyle;
        private MapObjectStyle _tankZorkiRObjectStyle;
        private MapObjectStyle _unknownZorkiRObjectStyle;


        private MapObjectStyle _unknownGlobusObjectStyle;

        private MapObjectStyle _unknownAeroscopeObjectStyle;
        private MapObjectStyle _homepointAeroscopeObjectStyle;
        private MapObjectStyle _pilotEscopeObjectStyle;


        private MapObjectStyle _cuirasseObjectStyle;
        private MapObjectStyle _cuirasseMainPointObjectStyle;
        private MapObjectStyle _cuirassePointObjectStyle;
        private MapObjectStyle _cuirasseOperatorObjectStyle;

        private MapObjectStyle _spoofingDirectionObjectStyle;

        private void UpdateSpoofing()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(_objectSpoofing);

                    if (SpoofingPosition.Latitude != -1 && SpoofingPosition.Longitude != -1 && VisibleSpoofingPosition)
                    _objectSpoofing = DrawObject(_spoofingObjectStyle, SpoofingPosition, "Spoof");
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void UpdateSpoofingTrack()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vSpoofingTrackObject != null)
                {
                    mapControl.RemoveObject(vSpoofingTrackObject.mapObject);
                    mapControl.RemoveObject(vSpoofingTrackObject.mapTrack);
                }

                vSpoofingTrackObject = new VDrone();
              
              

                _spoofingDirectionObjectStyle.Style.SymbolRotation = SpoofingTrack.Angle;

                vSpoofingTrackObject.mapObject = DrawObject(_spoofingDirectionObjectStyle, SpoofingTrack.LocationDrone, "");
                vSpoofingTrackObject.mapTrack = (SpoofingTrack.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, SpoofingTrack.LocationTrack, null, SpoofingTrack.PenTrack, 2) : null;


            }), DispatcherPriority.Background);

        }

        private void UpdateStartPosition()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(_objectStartPosition);

                    if (StartPointRout.Latitude != -1 && StartPointRout.Longitude != -1)
                        _objectStartPosition = DrawObject(_startPointObjectStyle, StartPointRout, "Start");
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void UpdateStopPosition(float Distance)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(_objectStopPosition);

                    if (StopPointRout.Latitude != -1 && StopPointRout.Longitude != -1)
                        _objectStopPosition = DrawObject(_stopPointObjectStyle, StopPointRout, Distance.ToString()+" m");
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void UpdateGNSS()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(_objectGNSS);

                    if (GNSSPosition.Latitude != -1 && GNSSPosition.Longitude != -1 && VisibleGNSSPosition)
                        _objectGNSS = DrawObject(_gnssObjectStyle, GNSSPosition, "GNSS");
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }
       
        private void UpdateJammers()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {

                    if (vJammerObject != null)
                    {
                        foreach (var jammer in vJammerObject)
                        {

                            mapControl.RemoveObject(jammer.mapJammer);
                            mapControl.RemoveObject(jammer.mapZoneAlarm);
                            mapControl.RemoveObject(jammer.mapZoneAttention);
                            mapControl.RemoveObject(jammer.mapZoneBearing);
                            mapControl.RemoveObject(jammer.mapZoneReadiness);
                            mapControl.RemoveObject(jammer.mapZoneOptic);
                            mapControl.RemoveObject(jammer.mapZoneJamming);
                            mapControl.RemoveObject(jammer.mapZoneJammingSecond);
                            mapControl.RemoveObject(jammer.mapZoneConnection);
                            mapControl.RemoveObject(jammer.mapZoneConnectionSecond);

                            mapControl.RemoveObject(jammer.mapZoneRotateJamming);
                            mapControl.RemoveObject(jammer.mapZoneRotateJammingSecond);
                            mapControl.RemoveObject(jammer.mapZoneRotateConnection);
                            mapControl.RemoveObject(jammer.mapZoneRotateConnectionSecond);

                            foreach (var b in jammer.mapBearing)
                                mapControl.RemoveObject(b);

                            mapControl.RemoveObject(jammer.mapDirection);
                            mapControl.RemoveObject(jammer.mapZoneStaticJamming);

                            
                        }

                        vJammerObject.Clear();
                    }

                    foreach (var jammer in Jammers.ToArray())
                    {
                        jammer.OnUpdateBearing += Jammer_OnUpdateBearing;
                        vJammerObject.Add(new VJammer(jammer.Id));

                        vJammerObject.Last().mapJammer = DrawObject(_jammerObjectStyle,
                                                      new Location(jammer.Coordinates.Longitude, jammer.Coordinates.Latitude, jammer.Coordinates.Altitude),
                                                                       jammer.Id.ToString());

                        
                        jammer.OnUpdateZone += ZoneUpdate;


                        jammer.ZoneAlarm.Radius = ZoneAlarm;
                        jammer.ZoneAlarm.Sector = 360;
                        jammer.ZoneAlarm.Visible = VisibleZoneCircle;
                        jammer.ZoneAlarm.PenZone = PenZoneAlarm;
                        jammer.ZoneAlarm.BrushZone = BrushZoneAlarm;


                        
                        jammer.ZoneAttention.Radius = ZoneAttention;
                        jammer.ZoneAttention.Sector = 360;
                        jammer.ZoneAttention.Visible = VisibleZoneCircle;
                        jammer.ZoneAttention.PenZone = PenZoneAttention;
                        jammer.ZoneAttention.BrushZone = BrushZoneAttention;
                        

                       
                        jammer.ZoneReadiness.Radius = ZoneReadiness;
                        jammer.ZoneReadiness.Sector = 360;
                        jammer.ZoneReadiness.Visible = VisibleZoneCircle;
                        jammer.ZoneReadiness.PenZone = PenZoneReadiness;
                        jammer.ZoneReadiness.BrushZone = BrushZoneReadiness;
                        

                        
                        jammer.ZoneBearing.Radius = ZoneBearing;
                        jammer.ZoneBearing.Sector = SectorBearing;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneBearing.Visible = VisibleZoneBearing;
                        else
                            jammer.ZoneBearing.Visible = false;

                        jammer.ZoneBearing.PenZone = PenZoneBearing;
                        jammer.ZoneBearing.BrushZone = BrushZoneBearing;
                        

                        
                        jammer.ZoneJamming.Radius = ZoneJamming;
                        jammer.ZoneJamming.Sector = SectorJamming;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneJamming.Visible = VisibleZoneJamming;
                        else
                            jammer.ZoneJamming.Visible = false;
                        jammer.ZoneJamming.PenZone = PenZoneJamming;
                        jammer.ZoneJamming.BrushZone = BrushZoneJamming;



                        jammer.ZoneJammingSecond.Radius = ZoneJamming;
                        jammer.ZoneJammingSecond.Sector = SectorJamming;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneJammingSecond.Visible = VisibleZoneJammingSecond;
                        else
                            jammer.ZoneJammingSecond.Visible = false;
                        jammer.ZoneJammingSecond.PenZone = PenZoneJammingSecond;
                        jammer.ZoneJammingSecond.BrushZone = BrushZoneJammingSecond;



                        jammer.ZoneConnection.Radius = ZoneConnect;
                        jammer.ZoneConnection.Sector = SectorConnect;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneConnection.Visible = VisibleZoneConnection;
                        else
                            jammer.ZoneConnection.Visible = false;
                        jammer.ZoneConnection.PenZone = PenZoneConnectLink;
                        jammer.ZoneConnection.BrushZone = BrushZoneConnectLink;



                        jammer.ZoneConnectionSecond.Radius = ZoneConnect;
                        jammer.ZoneConnectionSecond.Sector = SectorConnect;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneConnectionSecond.Visible = VisibleZoneConnectionSecond;
                        else
                            jammer.ZoneConnectionSecond.Visible = false;
                        jammer.ZoneConnectionSecond.PenZone = PenZoneConnectPC;
                        jammer.ZoneConnectionSecond.BrushZone = BrushZoneConnectPC;



                        jammer.ZoneOptic.Radius = ZoneOptic;
                        jammer.ZoneOptic.Sector = 40;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneOptic.Visible = VisibleZoneOptic;
                        else
                            jammer.ZoneOptic.Visible = false;
                        jammer.ZoneOptic.PenZone = PenZoneOptic;
                        jammer.ZoneOptic.BrushZone = BrushZoneOptic;


                        jammer.ZoneRotate.Radius = ZoneJamming;
                        jammer.ZoneRotate.Sector = SectorJamming;
                        jammer.ZoneRotate.Visible = false;
                        jammer.ZoneRotate.PenZone = PenZoneRotateJamming;
                        jammer.ZoneRotate.BrushZone = BrushZoneRotateJamming;

                        jammer.ZoneRotateJamming.Radius = ZoneJamming;
                        jammer.ZoneRotateJamming.Sector = SectorJamming;
                        jammer.ZoneRotateJamming.Visible = false;
                        jammer.ZoneRotateJamming.PenZone = PenZoneRotateJamming;
                        jammer.ZoneRotateJamming.BrushZone = BrushZoneRotateJamming;

                        jammer.ZoneRotateJammingSecond.Radius = ZoneJamming;
                        jammer.ZoneRotateJammingSecond.Sector = SectorJamming;
                        jammer.ZoneRotateJammingSecond.Visible = false;
                        jammer.ZoneRotateJammingSecond.PenZone = PenZoneRotateJammingSecond;
                        jammer.ZoneRotateJammingSecond.BrushZone = BrushZoneRotateJammingSecond;

                        jammer.ZoneRotateConnection.Radius = ZoneConnect;
                        jammer.ZoneRotateConnection.Sector = SectorConnect;
                        jammer.ZoneRotateConnection.Visible = false;
                        jammer.ZoneRotateConnection.PenZone = PenZoneRotateLink;
                        jammer.ZoneRotateConnection.BrushZone = BrushZoneRotateLink;

                        jammer.ZoneRotateConnectionSecond.Radius = ZoneConnect;
                        jammer.ZoneRotateConnectionSecond.Sector = SectorConnect;
                        jammer.ZoneRotateConnectionSecond.Visible = false;
                        jammer.ZoneRotateConnectionSecond.PenZone = PenZoneRotatePC;
                        jammer.ZoneRotateConnectionSecond.BrushZone = BrushZoneRotatePC;

                        jammer.ZoneDirection.Radius = 20000;
                        jammer.ZoneDirection.Sector = 0;
                        if (jammer.Role == StationRole.Own)
                            jammer.ZoneDirection.Visible = VisibleDirection;
                        else
                            jammer.ZoneDirection.Visible = false;
                        jammer.ZoneDirection.PenZone = PenZoneDirection;
                        jammer.ZoneDirection.BrushZone = BrushZoneDirection;


                        jammer.ZoneStaticJamming.Radius = StaticZoneJamming;
                        jammer.ZoneStaticJamming.Sector = 360;
                        if (jammer.Role == StationRole.Own && StaticZoneJamming > 0)
                            jammer.ZoneStaticJamming.Visible = true;
                        else
                            jammer.ZoneStaticJamming.Visible = false;
                        jammer.ZoneStaticJamming.PenZone = PenZoneJamming;
                        jammer.ZoneStaticJamming.BrushZone = BrushZoneJamming;
                        jammer.ZoneStaticJamming.BrushZoneLight = BrushZoneJammingLight;
                        

                        Jammer_OnUpdateBearing(jammer, new EventArgs());

                        


                    }

                    

                }), DispatcherPriority.Background);

            }

            catch
            { }

        }

        private void ZoneUpdate(object sender, string e)
        {

            var jammer = (VMJammer)sender;

            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    switch (e)
                    {
                        case "Alarm":

                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneAlarm);

                            if (jammer.ZoneAlarm.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneAlarm = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneAlarm.LocationZone, jammer.ZoneAlarm.BrushZone, jammer.ZoneAlarm.PenZone, 2);

                            
                            break;

                        case "Readiness":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneReadiness);

                            if (jammer.ZoneReadiness.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneReadiness = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneReadiness.LocationZone, jammer.ZoneReadiness.BrushZone, jammer.ZoneReadiness.PenZone, 2);
                            break;

                        case "Attention":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneAttention);

                            if (jammer.ZoneAttention.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneAttention = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneAttention.LocationZone, jammer.ZoneAttention.BrushZone, jammer.ZoneAttention.PenZone, 2);
                            break;

                        case "Bearing":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneBearing);

                            if (jammer.ZoneBearing.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneBearing = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneBearing.LocationZone, jammer.ZoneBearing.BrushZone, jammer.ZoneBearing.PenZone, 2);
                            break;

                        case "Jamming":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneJamming);

                            if (jammer.ZoneJamming.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneJamming = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneJamming.LocationZone, jammer.ZoneJamming.BrushZone, jammer.ZoneJamming.PenZone, 2);
                            break;

                        case "JammingSecond":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneJammingSecond);

                            if (jammer.ZoneJammingSecond.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneJammingSecond = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneJammingSecond.LocationZone, jammer.ZoneJammingSecond.BrushZone, jammer.ZoneJammingSecond.PenZone, 2);
                            break;

                        case "Connection":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneConnection);

                            if (jammer.ZoneConnection.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneConnection = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneConnection.LocationZone, jammer.ZoneConnection.BrushZone, jammer.ZoneConnection.PenZone, 2);
                            break;

                        case "ConnectionSecond":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneConnectionSecond);

                            if (jammer.ZoneConnectionSecond.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneConnectionSecond = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneConnectionSecond.LocationZone, jammer.ZoneConnectionSecond.BrushZone, jammer.ZoneConnectionSecond.PenZone, 2);
                            break;

                        case "Optic":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneOptic);

                            if (jammer.ZoneOptic.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneOptic = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneOptic.LocationZone, jammer.ZoneOptic.BrushZone, jammer.ZoneOptic.PenZone, 2);
                            break;
                     
                        case "RotateJamming":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateJamming);

                            if (jammer.ZoneRotateJamming.Visible && jammer.ZoneJamming.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateJamming = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotateJamming.LocationZone, jammer.ZoneRotateJamming.BrushZone, jammer.ZoneRotateJamming.PenZone, 2);
                            break;

                        case "RotateJammingSecond":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateJammingSecond);

                            
                            if (jammer.ZoneRotateJammingSecond.Visible && jammer.ZoneJammingSecond.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateJammingSecond = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotateJammingSecond.LocationZone, jammer.ZoneRotateJammingSecond.BrushZone, jammer.ZoneRotateJammingSecond.PenZone, 2);
                        
                            break;

                        case "RotateConnection":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateConnection);

                            if (jammer.ZoneRotateConnection.Visible && jammer.ZoneConnection.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateConnection = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotateConnection.LocationZone, jammer.ZoneRotateConnection.BrushZone, jammer.ZoneRotateConnection.PenZone, 2);
                            break;

                        case "RotateConnectionSecond":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateConnectionSecond);

                            if (jammer.ZoneRotateConnectionSecond.Visible && jammer.ZoneConnectionSecond.Visible)
                                vJammerObject.Find(x => x.ID == jammer.Id).mapZoneRotateConnectionSecond = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneRotateConnectionSecond.LocationZone, jammer.ZoneRotateConnectionSecond.BrushZone, jammer.ZoneRotateConnectionSecond.PenZone, 2);
                            break;


                        case "Direction":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapDirection);

                            if (jammer.ZoneDirection.Visible && VisibleDirection )
                                vJammerObject.Find(x => x.ID == jammer.Id).mapDirection = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneDirection.LocationZone, jammer.ZoneDirection.BrushZone, jammer.ZoneDirection.PenZone, 2);
                            break;


                        case "StaticJamming":
                            mapControl.RemoveObject(vJammerObject.Find(x => x.ID == jammer.Id).mapZoneStaticJamming);

                            if (jammer.ZoneStaticJamming.Visible)
                            {
                                if (jammer.Regime == EMapMode.JAMMING)
                                    vJammerObject.Find(x => x.ID == jammer.Id).mapZoneStaticJamming = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneStaticJamming.LocationZone, jammer.ZoneStaticJamming.BrushZone, jammer.ZoneStaticJamming.PenZone, 2);
                                else
                                    vJammerObject.Find(x => x.ID == jammer.Id).mapZoneStaticJamming = DrawFeaturePolygon(TypeFeature.Polygon, jammer.ZoneStaticJamming.LocationZone, jammer.ZoneStaticJamming.BrushZoneLight, jammer.ZoneStaticJamming.PenZone, 2);
                            }
                                
                            break;


                    }


                }), DispatcherPriority.Background);
            }

            catch
            { }
        }

        private void ZoneUpdateGrozaR(object sender, EventArgs e)
        {
            var grozaR = (VMGrozaR)sender;

            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    mapControl.RemoveObject(vGrozaRObject.Find(x => x.ID == grozaR.ID).mapZoneJamming);

                    if (grozaR.ZoneJamming.Visible)
                    {
                        vGrozaRObject.Find(x => x.ID == grozaR.ID).mapZoneJamming = DrawFeaturePolygon(TypeFeature.Polygon,
                                                                                  grozaR.ZoneJamming.LocationZone, grozaR.ZoneJamming.BrushZone,
                                                                                  grozaR.ZoneJamming.PenZone, 2);
                    }


                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void Jammer_OnUpdateBearing(object sender, EventArgs e)
        {
            var jammer = (VMJammer)sender;
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {

                    foreach (var tt in vJammerObject.Find(x => x.ID == jammer.Id).mapBearing.ToList())
                        mapControl.RemoveObject(tt);

                    vJammerObject.Find(x => x.ID == jammer.Id).mapBearing.Clear();

                    foreach (var b in jammer.BearingSource)
                    {
                        if (b.Visible && b.Direction > -1)
                            vJammerObject.Find(x => x.ID == jammer.Id).mapBearing.Add(DrawFeaturePolygon(TypeFeature.Polygon, b.locationBearing, null, b.PenBearing, 3));
                    }


                }), DispatcherPriority.Background);
            }

            catch
            { }
        }

        private void UpdateDrones()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vDroneObject != null)
                    foreach (var d in vDroneObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vDroneObject = new List<VDrone>(Drones.Count);


                foreach (var drone in Drones)
                {

                    if (drone.Visible)
                    {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = drone.IDSource;

                        switch (drone.View)
                        {
                            case ViewUAV.Multicopter:
                                tempdrone.mapObject = DrawObject(_multicopterObjectStyle, drone.LocationDrone, drone.Name);
                                break;

                            case ViewUAV.Plane:
                                tempdrone.mapObject = DrawObject(_planeObjectStyle, drone.LocationDrone, drone.Name);
                                break;

                            case ViewUAV.GSM:
                                tempdrone.mapObject = DrawObject(_gsmObjectStyle, drone.LocationDrone, drone.Name);
                                break;

                            default:
                                tempdrone.mapObject = DrawObject(_unknownObjectStyle, drone.LocationDrone, drone.Name);
                                break;


                        }

                        tempdrone.mapTrack = (drone.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, drone.LocationTrack, null, drone.PenTrack, 2) : null;

                        vDroneObject.Add(tempdrone);


                    }
                        

                    
                }
            }), DispatcherPriority.Background);


            UpdateTransparentSource();
        }

        private void UpdateRodnikTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vRodnikObject != null)
                    foreach (var d in vRodnikObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vRodnikObject = new List<VDrone>(RodnikTargets.Count);


                foreach (var rodnikTargets in RodnikTargets)
                {

                    if (rodnikTargets.Visible)
                    {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = rodnikTargets.IDSource;

                        switch (rodnikTargets.View)
                        {
                            case ViewRodnik.Ballistic:
                                tempdrone.mapObject = DrawObject(_ballisticRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            case ViewRodnik.Balloon:
                                tempdrone.mapObject = DrawObject(_balloonRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            case ViewRodnik.Helicopter:
                                tempdrone.mapObject = DrawObject(_helicopterRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            case ViewRodnik.Plane:
                                tempdrone.mapObject = DrawObject(_planeRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            case ViewRodnik.Unknown:
                                tempdrone.mapObject = DrawObject(_unknownRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;

                            default:
                                tempdrone.mapObject = DrawObject(_unknownRodnikObjectStyle, rodnikTargets.LocationDrone, rodnikTargets.Name);
                                break;


                        }

                        tempdrone.mapTrack = (rodnikTargets.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, rodnikTargets.LocationTrack, null, rodnikTargets.PenTrack, 2) : null;

                        vRodnikObject.Add(tempdrone);

                    }



                }
            }), DispatcherPriority.Background);

        }

        private void UpdateZorkiRTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vZorkiRObject != null)
                    foreach (var d in vZorkiRObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vZorkiRObject = new List<VDrone>(ZorkiRTargets.Count);


                foreach (var zorkiRTargets in ZorkiRTargets)
                {


                    bool VisibleTarget(ViewZorkiR key)
                    {
                        bool res = false;
                        bool hasValue = ZorkiRTargetVisible.TryGetValue(key, out res);

                        if (!hasValue)
                            return false;
                        else
                            return res;

                    }

                    bool visibleTemp = (VisibleTarget(zorkiRTargets.View));

                   
                    if (visibleTemp)
                    {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = zorkiRTargets.IDSource;

                        switch (zorkiRTargets.View)
                        {
                            case ViewZorkiR.Car:
                                tempdrone.mapObject = DrawObject(_carZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Group:
                                tempdrone.mapObject = DrawObject(_groupZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Helicopter:
                                tempdrone.mapObject = DrawObject(_helicopterZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Human:
                                tempdrone.mapObject = DrawObject(_humanZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.IFV:
                                tempdrone.mapObject = DrawObject(_IFVZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Tank:
                                tempdrone.mapObject = DrawObject(_tankZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.UAV:
                                tempdrone.mapObject = DrawObject(_UAVZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            case ViewZorkiR.Unknown:
                                tempdrone.mapObject = DrawObject(_unknownZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                            default:
                                tempdrone.mapObject = DrawObject(_unknownZorkiRObjectStyle, zorkiRTargets.LocationDrone, zorkiRTargets.Name);
                                break;

                        }

                        tempdrone.mapTrack = (zorkiRTargets.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, zorkiRTargets.LocationTrack, null, zorkiRTargets.PenTrack, 2) : null;

                        vZorkiRObject.Add(tempdrone);
                    }



                }
            }), DispatcherPriority.Background);


        }

        private void UpdateGlobusTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vGlobusObject != null)
                    foreach (var d in vGlobusObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vGlobusObject = new List<VDrone>(GlobusTargets.Count);


                foreach (var globusTargets in GlobusTargets)
                {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = globusTargets.IDSource;

                        tempdrone.mapObject = DrawObject(_unknownGlobusObjectStyle, globusTargets.LocationDrone, globusTargets.Name);
                   
                        tempdrone.mapTrack = (globusTargets.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, globusTargets.LocationTrack, null, globusTargets.PenTrack, 2) : null;

                        vGlobusObject.Add(tempdrone);                    

                }
            }), DispatcherPriority.Background);


        }


        private void UpdateAeroscopeTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vAeroscopeObject != null)
                    foreach (var d in vAeroscopeObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }
                if (vAeroscopeHomeObject != null)
                    foreach (var d in vAeroscopeHomeObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                    }
                

                vAeroscopeObject = new List<VDrone>(AeroscopeTargets.Count);
                vAeroscopeHomeObject = new List<VMStaticObject>(AeroscopeTargets.Count);


                foreach (var aeroscopeTargets in AeroscopeTargets)
                {

                    VDrone tempdrone = new VDrone();
                    tempdrone.IDSource = aeroscopeTargets.IDSource;

                    tempdrone.mapObject = DrawObject(_unknownAeroscopeObjectStyle, aeroscopeTargets.LocationDrone, aeroscopeTargets.Type);

                    tempdrone.mapTrack = (aeroscopeTargets.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, aeroscopeTargets.LocationTrack, null, aeroscopeTargets.PenTrack, 2) : null;

                    vAeroscopeObject.Add(tempdrone);

                    VMStaticObject home = new VMStaticObject();
                    home.mapObject = DrawObject(_homepointAeroscopeObjectStyle, aeroscopeTargets.HomePoint, aeroscopeTargets.Type);
                    vAeroscopeHomeObject.Add(home);
                }
            }), DispatcherPriority.Background);


        }

        private void UpdateEscopeTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (this.vEscopeObject != null)
                        foreach (var d in vEscopeObject)
                        {
                            mapControl.RemoveObject(d.mapObject);
                            mapControl.RemoveObject(d.mapTrack);
                        }
                    if (vEscopeHomeObject != null)
                        foreach (var d in vEscopeHomeObject)
                        {
                            mapControl.RemoveObject(d.mapObject);
                        }
                    if (this.vEscopePilotObject != null)
                        foreach (var d in vEscopePilotObject)
                        {
                            mapControl.RemoveObject(d.mapObject);
                        }
                    //TODO: мб переделать под один foreach

                    this.vEscopeObject = new List<VDrone>(EscopeTargets.Count);
                    this.vEscopeHomeObject = new List<VMStaticObject>(EscopeTargets.Count);
                    this.vEscopePilotObject = new List<VMStaticObject>(EscopeTargets.Count);

                    foreach (var targets in EscopeTargets)
                    {
                        VDrone tempdrone = new VDrone();
                        //tempdrone.IDSource = targets.IDSource; //TODO: чет придумать?, нет номера

                        tempdrone.mapObject = DrawObject(_unknownAeroscopeObjectStyle, targets.LocationDrone, targets.Type);
                        tempdrone.mapTrack = (targets.VisibleTrack) ? DrawFeaturePolygon(TypeFeature.Polyline, targets.LocationTrack, null, targets.PenTrack, 2) : null;

                        vEscopeObject.Add(tempdrone);

                        VMStaticObject home = new VMStaticObject();
                        home.mapObject = DrawObject(_homepointAeroscopeObjectStyle, targets.HomePoint, "Home");
                        vEscopeHomeObject.Add(home);

                        VMStaticObject pilot = new VMStaticObject();
                        pilot.mapObject = DrawObject(this._pilotEscopeObjectStyle, targets.Pilot, "Pilot");
                        vEscopePilotObject.Add(pilot);
                    }
                }), DispatcherPriority.Background);


        }

        private void UpdateCuirasseTargets()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vCuirasseObject != null)
                    foreach (var d in vCuirasseObject)
                    {
                        mapControl.RemoveObject(d.mapObject);
                        mapControl.RemoveObject(d.mapTrack);
                    }


                vCuirasseObject = new List<VDrone>(CuirasseTargets.Count);


                foreach (var cuirasseTargets in CuirasseTargets)
                {
                    
                    if (VisibleCuirasseTarget)
                    {

                        VDrone tempdrone = new VDrone();
                        tempdrone.IDSource = cuirasseTargets.ID;

                        tempdrone.mapObject = DrawObject(_cuirasseObjectStyle, cuirasseTargets.LocationDrone, "");

                        tempdrone.mapTrack = DrawFeaturePolygon(TypeFeature.Polyline, cuirasseTargets.LocationTrack, null, cuirasseTargets.PenTrack, 2);

                        vCuirasseObject.Add(tempdrone);
                    }

                }
            }), DispatcherPriority.Background);


        }

        private void UpdateCuirassePoints()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (vCuirassePoints != null)
                    foreach (var d in vCuirassePoints)
                        mapControl.RemoveObject(d.mapObject);

                vCuirassePoints = new List<VMStaticObject>(CuirassePoints.Count);

                foreach (var cuirassePoints in CuirassePoints)
                {
                    if (VisibleCuirasseTarget)
                    {
                        VMStaticObject tempPoint = new VMStaticObject();
                        tempPoint.IDSource = cuirassePoints.ID;

                        switch (tempPoint.IDSource)
                        {
                            case (byte)ViewCuirassePoint.Operator:
                                tempPoint.mapObject = DrawObject(_cuirasseOperatorObjectStyle, cuirassePoints.LocationPoint, "");
                                break;

                            case (byte)ViewCuirassePoint.MainStation:
                                tempPoint.mapObject = DrawObject(_cuirasseMainPointObjectStyle, cuirassePoints.LocationPoint, "");
                                break;

                            default:
                                tempPoint.mapObject = DrawObject(_cuirassePointObjectStyle, cuirassePoints.LocationPoint, "");
                                break;
                        }
                                         
                        vCuirassePoints.Add(tempPoint);
                    }
                }
            }), DispatcherPriority.Background);
        }




        private void UpdateGrozaR()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {

                    if (vGrozaRObject != null)
                    {
                        foreach (var grozaR in vGrozaRObject)
                        {

                            mapControl.RemoveObject(grozaR.mapGrozaR);
                            mapControl.RemoveObject(grozaR.mapZoneJamming);
                           
                        }

                        vGrozaRObject.Clear();
                    }

                    foreach (var grozaR in GrozaROperators)
                    {

                        vGrozaRObject.Add(new VGrozaR(grozaR.ID));

                        if (grozaR.LocationPoint.Longitude > -1 && grozaR.LocationPoint.Latitude > -1)
                        {

                            vGrozaRObject.Last().mapGrozaR = DrawObject(_grozaRObjectStyle,
                                                          new Location(grozaR.LocationPoint.Longitude, grozaR.LocationPoint.Latitude, grozaR.LocationPoint.Altitude),
                                                                           grozaR.Note.ToString());

                            grozaR.OnUpdateZone += ZoneUpdateGrozaR;

                            grozaR.ZoneJamming.Direction = grozaR.Azimuth;
                            grozaR.ZoneJamming.Radius = 5000;
                            grozaR.ZoneJamming.Sector = 20;
                            grozaR.ZoneJamming.Visible = grozaR.Azimuth > -1 && grozaR.Azimuth < 361 && grozaR.State != EStateGrozaR.NoConnect;
                            grozaR.ZoneJamming.PenZone = (grozaR.State==EStateGrozaR.NoRadiation)? PenZoneGrozaR: PenZoneJammingGrozaR;
                            grozaR.ZoneJamming.BrushZone = (grozaR.State == EStateGrozaR.NoRadiation) ? BrushZoneGrozaR : BrushZoneJammingGrozaR;
                        }

                    }



                }), DispatcherPriority.Background);

            }

            catch
            { }

        }



        private void InitializeObjectStyles()
        {
            
            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));

            try
            {
                Bitmap imgSpoofing;

                if (this.PathImageSpoofing != null && this.PathImageSpoofing != "" && File.Exists(this.PathImageSpoofing))
                {
                    imgSpoofing = new Bitmap(this.PathImageSpoofing);
                }
                else
                {
                    imgSpoofing = (Bitmap)rm.GetObject("Spoofing");
                }

                _spoofingObjectStyle = mapControl.LoadObjectStyle(imgSpoofing,
                        scale: 0.2, objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {
                Bitmap imgSpoofingTrack = (Bitmap)rm.GetObject("SpoofingTrack");

                _spoofingTrackObjectStyle = mapControl.LoadObjectStyle(imgSpoofingTrack,
                        scale: 0.2, objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try 
            {
                Bitmap imgGNSS;

                if (this.PathImageGNSS != null && this.PathImageGNSS != "" && File.Exists(this.PathImageGNSS))
                {
                    imgGNSS = new Bitmap(this.PathImageGNSS);
                }
                else
                {
                    imgGNSS = (Bitmap)rm.GetObject("GNSS");
                }
                

                _gnssObjectStyle = mapControl.LoadObjectStyle(imgGNSS,
                        scale: 0.8,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }


            try
            {
                Bitmap imgJammer;

                if (this.PathImageJammer != null && this.PathImageJammer != "" && File.Exists(this.PathImageJammer))
                {
                    imgJammer = new Bitmap(this.PathImageJammer);
                }
                else
                {
                    imgJammer = (Bitmap)rm.GetObject("Jammer");
                }
                
                _jammerObjectStyle = mapControl.LoadObjectStyle(imgJammer,
                        scale: 1.5,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {
                Bitmap imgCopter = (Bitmap)rm.GetObject("Copter");

                _multicopterObjectStyle = mapControl.LoadObjectStyle(imgCopter,
                        scale: 0.3,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {
                Bitmap imgUnknown = (Bitmap)rm.GetObject("Unknown");

                _unknownObjectStyle = mapControl.LoadObjectStyle(imgUnknown,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                

                Bitmap imgPlane = (Bitmap)rm.GetObject("Plane");

                _planeObjectStyle = mapControl.LoadObjectStyle(imgPlane,
                        scale: 0.3,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {


                Bitmap imgGSM = (Bitmap)rm.GetObject("GSM");

                _gsmObjectStyle = mapControl.LoadObjectStyle(imgGSM,
                        scale: 0.3,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            

            try
            {
                Bitmap imgStartPoint = (Bitmap)rm.GetObject("StartPoint");

                _startPointObjectStyle = mapControl.LoadObjectStyle(imgStartPoint,
                    scale: 0.3,
                    objectOffset: new Mapsui.Styles.Offset(0, 10),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }

            try
            {
                Bitmap imgStopPoint = (Bitmap)rm.GetObject("StopPoint");

                _stopPointObjectStyle = mapControl.LoadObjectStyle(imgStopPoint,
                    scale: 0.06,
                    objectOffset: new Mapsui.Styles.Offset(0, 10),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }


            try
            {
                Bitmap imgGrozaR = (Bitmap)rm.GetObject("shooter");

                _grozaRObjectStyle = mapControl.LoadObjectStyle(imgGrozaR,
                    scale: 0.06,
                    objectOffset: new Mapsui.Styles.Offset(0, 10),
                    textOffset: new Mapsui.Styles.Offset(0, 12));
            }
            catch { }


            
        }

        private void InitializeRodnikObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));

           

            #region Rodnik
            try
            {
                Bitmap imgBallisticRodnik = (Bitmap)rm.GetObject("BombRodnik");

                _ballisticRodnikObjectStyle = mapControl.LoadObjectStyle(imgBallisticRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgBalloonRodnik = (Bitmap)rm.GetObject("BalloonRodnik");

                _balloonRodnikObjectStyle = mapControl.LoadObjectStyle(imgBalloonRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgHelicopterRodnik = (Bitmap)rm.GetObject("HelicopterRodnik");

                _helicopterRodnikObjectStyle = mapControl.LoadObjectStyle(imgHelicopterRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgPlaneRodnik = (Bitmap)rm.GetObject("PlaneRodnik");

                _planeRodnikObjectStyle = mapControl.LoadObjectStyle(imgPlaneRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgUnknownRodnik = (Bitmap)rm.GetObject("UnknownRodnik");

                _unknownRodnikObjectStyle = mapControl.LoadObjectStyle(imgUnknownRodnik,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            #endregion
        }

        private void InitializeZorkiRObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));



            #region ZorkiR
            try
            {
                Bitmap imgHumanZorkiR = (Bitmap)rm.GetObject("HumanZorkiR");

                _humanZorkiRObjectStyle = mapControl.LoadObjectStyle(imgHumanZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgCarZorkiR = (Bitmap)rm.GetObject("CarZorkiR");

                _carZorkiRObjectStyle = mapControl.LoadObjectStyle(imgCarZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgHelicopterZorkiR = (Bitmap)rm.GetObject("HelicopterZorkiR");

                _helicopterZorkiRObjectStyle = mapControl.LoadObjectStyle(imgHelicopterZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgGroupZorkiR = (Bitmap)rm.GetObject("GroupZorkiR");

                _groupZorkiRObjectStyle = mapControl.LoadObjectStyle(imgGroupZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgUAVZorkiR = (Bitmap)rm.GetObject("UAVZorkiR");

                _UAVZorkiRObjectStyle = mapControl.LoadObjectStyle(imgUAVZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgTankZorkiR = (Bitmap)rm.GetObject("TankZorkiR");

                _tankZorkiRObjectStyle = mapControl.LoadObjectStyle(imgTankZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgIFVZorkiR = (Bitmap)rm.GetObject("BMPZorkiR");

                _IFVZorkiRObjectStyle = mapControl.LoadObjectStyle(imgIFVZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgUnknownZorkiR = (Bitmap)rm.GetObject("UnknownZorkiR");

                _unknownZorkiRObjectStyle = mapControl.LoadObjectStyle(imgUnknownZorkiR,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            #endregion
        }


        private void InitializeGlobusObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));



            #region Globus
            try
            {
                Bitmap imgUnknownGlobus = (Bitmap)rm.GetObject("UnknownGlobus");

                _unknownGlobusObjectStyle = mapControl.LoadObjectStyle(imgUnknownGlobus,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }
            #endregion
        }

        private void InitializeAeroscopeObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));



            #region Aeroscope
            try
            {
                Bitmap imgUnknownAeroscope = (Bitmap)rm.GetObject("UAVZorkiR");

                _unknownAeroscopeObjectStyle = mapControl.LoadObjectStyle(imgUnknownAeroscope,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));

                Bitmap imghomepointAeroscope = (Bitmap)rm.GetObject("helipad");
                _homepointAeroscopeObjectStyle = mapControl.LoadObjectStyle(imghomepointAeroscope,
                    scale: 0.04,
                    objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
                Bitmap imgPilotAeroscope = (Bitmap)rm.GetObject("controller");
                _pilotEscopeObjectStyle = mapControl.LoadObjectStyle(imgPilotAeroscope,
                    scale: 0.04,
                    objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }
            #endregion
        }

        private void InitializeCuirasseObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));



            #region Cuirasse
            try
            {
                Bitmap imgDroneCuirasse = (Bitmap)rm.GetObject("UAVZorkiR");

                _cuirasseObjectStyle = mapControl.LoadObjectStyle(imgDroneCuirasse,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgPointCuirasse = (Bitmap)rm.GetObject("CuirassePoint");

                _cuirassePointObjectStyle = mapControl.LoadObjectStyle(imgPointCuirasse,
                        scale: 0.7,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }

            try
            {
                Bitmap imgMainPointCuirasse = (Bitmap)rm.GetObject("CuirassePoint");

                _cuirasseMainPointObjectStyle = mapControl.LoadObjectStyle(imgMainPointCuirasse,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }


            try
            {
                Bitmap imgOperatorCuirasse = (Bitmap)rm.GetObject("CuirasseOperator");

                _cuirasseOperatorObjectStyle = mapControl.LoadObjectStyle(imgOperatorCuirasse,
                        scale: 0.25,
                        objectOffset: new Mapsui.Styles.Offset(0, 0),
                    textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }



            #endregion
        }

        private void InitializeSpoofingDirectionObjectStyles()
        {

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + 
                ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));



           
            try
            {
                Bitmap imgSpoofingDirection = (Bitmap)rm.GetObject("SpoofingDirection");

                _spoofingDirectionObjectStyle = mapControl.LoadObjectStyle(imgSpoofingDirection,
                           scale: 0.25,
                           objectOffset: new Mapsui.Styles.Offset(0, 0),
                       textOffset: new Mapsui.Styles.Offset(0, 5));
            }
            catch { }
                
        }

        public async void OpenMap()
        {
            try
            {
                if (PathMapTile != "" && PathMap != "")
                {
                    mapControl.DataFolder = PathMapTile;
                    await mapControl.OpenMap(PathMap);
                    SliderZoom.Maximum = mapControl.MaxResolution;
                    SliderZoom.Minimum = mapControl.MinResolution;
                }

           
            }
            catch 
            {
                
            }


        }

        public void CloseMap()
        {
            mapControl.CloseMap();

        }

        public void OpenMatrix()
        {
            try
            {
                if (PathMatrix != "")
                {
                    string[] files = Directory.GetFiles(PathMatrix);
                    mapControl.OpenDted(files);
                    
                }


            }
            catch
            {

            }


        }
        
        private void CenterMapGNSS()
        {
            try
            {
                CenterCoordMap(GNSSPosition);              
            }
            catch { }
        }

        private void CenterCoordMap(Location location)
        {
            try
            {
                
                mapControl.NavigateTo(ConvertToPoint( location));
            }
            catch { }
        }

        private void UpdateOrientationImage()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ImageDirection.PathImage = PathImageOrientation;
                    //if (PathImageOrientation != null && PathImageOrientation != "")
                    //    CarImg.Source = new BitmapImage(new Uri(PathImageOrientation));
                   

                }), DispatcherPriority.Background);
            }

            catch
            { }
        }

        private void UpdateOrientationAngle()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ImageDirection.Angle = AngleOrientation;

                    if (Jammers != null && Jammers.Count > 0 && Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                    {
                        Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneDirection.Direction = AngleOrientation;
                    }

                }), DispatcherPriority.Background);
            }

            catch
            { }
        }

      

        private void UpdateTransparentSource()
        {
            Zone1TDF = CountZoneSource(1);
            Zone2TDF = CountZoneSource(2);
            Zone3TDF = CountZoneSource(3);

        }

        private byte CountZoneSource(byte zone)
        {
            byte _count =0;
            foreach (var drone in Drones)
                if (drone.Zone == zone)
                    _count++;

            return _count;
        }

        private void DrawRout()
        {

            Dispatcher.BeginInvoke(new Action(() =>
            {
                mapControl.RemoveObject(_featureRout);

                
            }), DispatcherPriority.Background);



            if (StartPointRout.Latitude != -1 && StartPointRout.Longitude != -1 &&
                StopPointRout.Latitude != -1 && StopPointRout.Longitude != -1 )
            {
                try
                {
                    List<Location> _locationRout = new List<Location>();
                    _locationRout.Add(new Location(StartPointRout.Latitude, StartPointRout.Longitude));
                    _locationRout.Add(new Location(StopPointRout.Latitude, StopPointRout.Longitude));

                    var dist = CalculateDistance(StartPointRout, StopPointRout);

                    UpdateStopPosition(dist);

                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                       

                        _featureRout = DrawFeaturePolygon(TypeFeature.Polygon, _locationRout, null, PenRout, 3);
                    }), DispatcherPriority.Background);


                    
                }
                catch
                { }
            }
        }

        private float CalculateDistance(Location Start, Location Finish)
        {
            float _distance = 0;
            if (Start.Latitude != -1 &&
                            Start.Longitude != -1 &&
                            Finish.Latitude != -1 &&
                            Finish.Longitude != -1)
                try
                {
                    
                       
                        
                        _distance = (float)Math.Round(ClassBearing.f_D_2Points(Start.Latitude,
                                Start.Longitude, Finish.Latitude, Finish.Longitude, 1), MidpointRounding.AwayFromZero);
                    
                }
                catch { }

            return _distance;
        }

    }
}
