﻿using GrozaSModelsDBLib;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using UIMap.Models;
using WpfMapControl;


namespace UIMap
{
    /// <summary>
    /// Interaction logic for RstMapView.xaml
    /// </summary>
    public partial class RstMapView : UserControl
    {


        System.Timers.Timer timer = new System.Timers.Timer();

        private bool DragMouse = false;
        Location locationZero = new Location(0, 0);



        public RstMapView()
        {
            InitializeComponent();

            InitializeObjectStyles();

            InitializeRodnikObjectStyles();

            InitializeZorkiRObjectStyles();

            InitializeGlobusObjectStyles();

            InitializeAeroscopeObjectStyles();

            InitializeCuirasseObjectStyles();

            InitializeSpoofingDirectionObjectStyles();


            //ImageDirection.Background = new SolidColorBrush(Color.FromArgb(90, 100, 100, 100));


            mapControl.MapMouseMoveEvent += OnMapMouseMove;
            mapControl.ResolutionChangedEvent += OnResolutionCahnged;
            mapControl.MapClickEvent += OnMapClick;
            mapControl.MouseLeftButtonDown += MapControl_MouseLeftButtonDown;
            mapControl.MouseLeftButtonUp += MapControl_MouseLeftButtonUp;
            mapControl.MouseMove += MapControl_MouseMove;





            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 300;
            //timer.Start();


            Coordinate = new Location();

            LocationPos = new LocationFormat();

            FormatViewCoord = FormatCoord.DD_MM_mm;


            Projection = MapProjection.Geo;

            Drones.CollectionChanged += Drones_CollectionChanged;
            Jammers.CollectionChanged += Jammers_CollectionChanged;


            SetResourceLanguage(MapLanguages.EN);


            InitZorkiRTargetsVisible();

            SetHandAngleBRD1.BRDColor = Brushes.Red;

            SetHandAngleBRD2.BRDColor = Brushes.Violet;



        }

        private void InitZorkiRTargetsVisible()
        {
            ZorkiRTargetVisible.Add(ViewZorkiR.Car, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Group, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Helicopter, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Human, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.IFV, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Tank, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.UAV, true);
            ZorkiRTargetVisible.Add(ViewZorkiR.Unknown, true);
        }


        private void Jammers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateJammers();
        }

        private void Drones_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdateDrones();
        }

        private void MapControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DragMouse = false;
        }

        private void MapControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMouse = true;
        }

        private void MapControl_MouseMove(object sender, MouseEventArgs e)
        {
            //if (DragMouse && !CenterCoord.Equals(locationZero))
            //    CenterCoord = locationZero;
        }


        private void OnMapMouseMove(object sender, Location location)
        {            
            Coordinate = location;
        }

        private void OnMapClick(object sender, MapMouseClickEventArgs e)
        {

            if (e.ClickedButton.ChangedButton == MouseButton.Right)
            {
                
                ClickCoord = e.Location;
            }

        }

        private void OnResolutionCahnged(object sender, double resolution)
        {

            try
            {
                SliderZoom.Value = SliderZoom.Maximum - resolution + SliderZoom.Minimum;

                CurrentScale = resolution;

            }
            catch { }

        }


        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                PathMap = openFileDialog.FileName;
                OpenMap();
            }
        }

        private void SliderZoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!mapControl.IsMapLoaded)
                return;

            mapControl.Resolution = SliderZoom.Maximum - SliderZoom.Value + SliderZoom.Minimum;

        
        }


        public void SetScale(double scale)
        {
            

            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    
                    if (!mapControl.IsMapLoaded)
                        return;

                   
                    mapControl.Resolution = scale;


                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            PathMap = "";
            CloseMap();
        }


        private void SetSpoofMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
                return;

            OnSpoofingPosition?.Invoke(this, ClickCoord);

            //SpoofingPosition = ClickCoord;

        }

        private void SetGNSSMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
                return;

            GNSSPosition = ClickCoord;
        }

        private void SetJammerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
                return;

            OnJammerPosition?.Invoke(this, ClickCoord);
        }


        void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Random random = new Random();

            foreach (var jam in Jammers)
            {
                jam.ZoneBearing.Direction = random.Next(1, 359);
                jam.ZoneJamming.Direction = random.Next(1, 359);
                jam.ZoneOptic.Direction = random.Next(1, 359);
            }
            //timer.Stop();
        }

        private void SetDroneMenuItem_Click(object sender, RoutedEventArgs e)
        {            
            timer.Enabled = !timer.Enabled;
        }

        private void CenterButton_Click(object sender, RoutedEventArgs e)
        {
            CenterMapGNSS();
        }

        private void SetDirectionMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
          
            OnDirection?.Invoke(this, GetBeraingClickCoord());
        }

        private void SetStartRoutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            StartPointRout = ClickCoord;

        }

        private void SetStopRoutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            StopPointRout = ClickCoord;
        }

        private void ClickRoutButton_Click(object sender, RoutedEventArgs e)
        {
            StartPointRout = new Location(-1, -1);
            StopPointRout = new Location(-1, -1);

        }

        private void OpenMatrixButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
           
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PathMatrix = dialog.SelectedPath;
                //OpenMatrix();

            }


        }

        private float GetBeraingClickCoord()
        {
            if (!mapControl.IsMapLoaded)
            {
                return -1;
            }
            var DirectionPosition = ClickCoord;

            var OwnPosition = Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().Coordinates;

            return Azimuth.CalcAzimuth(OwnPosition.Latitude, OwnPosition.Longitude, ClickCoord.Latitude, ClickCoord.Longitude);
        }

        private void SetDirectionJammingMenuItem_Click(object sender, RoutedEventArgs e)
        {
           float angle = GetBeraingClickCoord();
           SetHandAngleBRD1.ValueAngle = angle;
           OnDirectionJamming?.Invoke(this, angle);
        }

        private void SetDirectionJammingSecondMenuItem_Click(object sender, RoutedEventArgs e)
        {
            float angle = GetBeraingClickCoord();
            SetHandAngleBRD2.ValueAngle = angle;
            OnDirectionJammingSecond?.Invoke(this, GetBeraingClickCoord());
        }

       
        private void SetDirectionConnectionMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnDirectionConnection?.Invoke(this, GetBeraingClickCoord());
        }

        private void SetDirectionConnectionSecondMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OnDirectionConnectionSecond?.Invoke(this, GetBeraingClickCoord());
        }

        private void SetDirectionHandJamming(object sender, float e)
        {
            OnDirectionJamming?.Invoke(this, e);
        }

        private void SetDirectionHandJammingSecond(object sender, float e)
        {
            OnDirectionJammingSecond?.Invoke(this, e);
        }

        private void SetZorkiRMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
                return;

            //OnZorkiRPosition?.Invoke(this, ClickCoord);
            OnZorkiRDirection?.Invoke(this, GetBeraingClickCoord());
        }

        private void SetDirectionGrozaRItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
                return;

            OnSetTargetForGrozaR?.Invoke(this, ClickCoord);
        }
    }
}
