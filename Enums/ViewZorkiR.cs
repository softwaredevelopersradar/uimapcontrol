﻿public enum ViewZorkiR : byte
{
    
    Human = 0,
    Car = 1,
    Helicopter = 2,
    Group = 3,
    UAV = 4,
    Tank = 5,
    IFV = 6,
    Unknown = 7,
    Human_manual = 8,
    Group_manual = 9,
    Car_manual = 10,
    MilitaryCar_manual = 11,
    Tank_manual = 12,
    Helicopter_manual = 13,    
    UAV_manual = 14,    
    Unknown_manual = 15
}