﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public class VJammer
    {
        public int ID { get; private set; }
        public IMapObject mapJammer { get; set; }

        public Feature mapZoneBearing { get; set; }
        public Feature mapZoneJamming { get; set; }

        public Feature mapZoneJammingSecond { get; set; }

        public Feature mapZoneConnection { get; set; }

        public Feature mapZoneConnectionSecond { get; set; }

        public Feature mapZoneOptic { get; set; }
        public Feature mapZoneAttention { get; set; }
        public Feature mapZoneReadiness { get; set; }
        public Feature mapZoneAlarm { get; set; }

        public Feature mapZoneRotate { get; set; }


        public Feature mapZoneRotateJamming { get; set; }
        public Feature mapZoneRotateJammingSecond { get; set; }
        public Feature mapZoneRotateConnection { get; set; }
        public Feature mapZoneRotateConnectionSecond { get; set; }


        public List<Feature> mapBearing { get; set; }


        public Feature mapDirection { get; set; }

        public Feature mapZoneStaticJamming { get; set; }

        public VJammer (int Id)
        {
            ID = Id;
            mapZoneBearing = new Feature();
            mapZoneJamming = new Feature();
            mapZoneJammingSecond = new Feature();
            mapZoneConnection = new Feature();
            mapZoneConnectionSecond = new Feature();
            mapZoneOptic = new Feature();
            mapZoneAttention = new Feature();
            mapZoneReadiness = new Feature();
            mapZoneAlarm = new Feature();
            mapZoneRotate = new Feature();

            mapZoneRotateJamming = new Feature();
            mapZoneRotateJammingSecond = new Feature();
            mapZoneRotateConnection = new Feature();
            mapZoneRotateConnectionSecond = new Feature();

            mapBearing = new List<Feature>();

            mapDirection = new Feature();

            mapZoneStaticJamming = new Feature();
        }


    }
}
