﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public  class VMGrozaR
    {
        public event EventHandler<EventArgs> OnUpdateZone;

        Location locNull = new Location(-1, -1);


        public VMGrozaR(int id, Location locationPoint, string note, short azimuth, EStateGrozaR state)
        {
            ID = id;
            LocationPoint = locationPoint;
            Note = note;
            Azimuth = azimuth;
            State = state;
            ZoneJamming = new VMZone("Jamming", new GrozaSModelsDBLib.Coord());
            ZoneJamming.OnUpdate += Zone_PropertyChanged;

            ZoneJamming.Center.Latitude = LocationPoint.Latitude;
            ZoneJamming.Center.Longitude = LocationPoint.Longitude;

            


        }

        private int _id;

        public int ID
        {
            get => _id;
            set
            {
                if (_id.Equals(value)) return;
                _id = value;

            }
        }


        public Mapsui.Styles.Pen PenTrack { get; set; }


        private Location _locationPoint;

        public Location LocationPoint
        {
            get => _locationPoint;
            set
            {
                if (_locationPoint.Equals(value)) return;
                _locationPoint = value;
                OnPropertyChanged();
            }
        }



        private string _note = "";

        public string Note
        {
            get { return _note; }
            set
            {
                if (_note.Equals(value)) return;
                _note = value;


            }
        }


        private short _azimuth = -1;

        public short Azimuth
        {
            get { return _azimuth; }
            set
            {
                if (_azimuth.Equals(value)) return;
                _azimuth = value;


            }
        }

        private EStateGrozaR _state = EStateGrozaR.NoConnect;

        public EStateGrozaR State
        {
            get { return _state; }
            set
            {
                if (_state.Equals(value)) return;
                _state = value;
            }
        }


        private VMZone _zoneJamming;



        public VMZone ZoneJamming
        {
            get { return _zoneJamming; }
            set
            {
                if (_zoneJamming != value)
                {
                    _zoneJamming = value;
                    OnPropertyChanged();
                   

                }
            }
        }


        private void Zone_PropertyChanged(object sender, EventArgs e)
        {
            VMZone temp = (VMZone)sender;

            OnUpdateZone?.Invoke(this, new EventArgs());
        }

        public VMGrozaR()
        {
            
        }



        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
