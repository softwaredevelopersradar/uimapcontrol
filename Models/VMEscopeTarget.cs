﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WpfMapControl;

namespace UIMap.Models
{
    public class VMEscopeTarget : INotifyPropertyChanged
    {
        //Location locNull = new Location(-1, -1);

        private List<Location> _locationTrack = new List<Location>();

        public List<Location> LocationTrack
        {
            get => _locationTrack;
            set
            {
                _locationTrack = value;
                OnPropertyChanged();
            }
        }


        private string _serialNumber = string.Empty;

        public string SerialNumber
        {
            get => _serialNumber;
            set
            {
                if (_serialNumber.Equals(value)) return;
                _serialNumber = value;

            }
        }

        private string _type = string.Empty;

        public string Type
        {
            get => _type;
            set
            {
                if (_type.Equals(value)) return;
                _type = value;

            }
        }


        private bool _visible = true;

        public bool Visible
        {
            get => _visible;
            set
            {
                if (_visible.Equals(value)) return;
                _visible = value;
                OnPropertyChanged();

            }
        }




        private bool _visibleTrack = false;

        public bool VisibleTrack
        {
            get => _visibleTrack;
            set
            {
                if (_visibleTrack.Equals(value)) return;
                _visibleTrack = value;
                OnPropertyChanged();

            }
        }

        public Mapsui.Styles.Pen PenTrack { get; set; }




        private Location _locationDrone;

        public Location LocationDrone
        {
            get => _locationDrone;
            set
            {
                if (_locationDrone.Equals(value)) return;
                _locationDrone = value;
                OnPropertyChanged();
            }
        }

        private Location _homePoint;

        public Location HomePoint
        {
            get => _homePoint;
            set
            {
                if (_homePoint.Equals(value)) return;
                _homePoint = value;
                OnPropertyChanged();
            }
        }

        private Location _pilot;

        public Location Pilot
        {
            get => _pilot;
            set
            {
                if (_pilot.Equals(value)) return;
                _pilot = value;
                OnPropertyChanged();
            }
        }

        public VMEscopeTarget()
        {
            PenTrack = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 255, 0, 255), 2);
            PenTrack.PenStyle = Mapsui.Styles.PenStyle.Dot;
        }

        public VMEscopeTarget(Mapsui.Styles.Color color)
        {
            PenTrack = new Mapsui.Styles.Pen(color, 2);
            PenTrack.PenStyle = Mapsui.Styles.PenStyle.Dot;
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
