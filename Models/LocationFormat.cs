﻿using CoordFormatLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace UIMap.Models
{
    public class LocationFormat: INotifyPropertyChanged
    {
        private VCFormat _coord = new VCFormat();

        public string LatitudePos { get; private set; }

        public string LongitudePos { get; private set; }


        public FormatCoord FormatView {get; set;}

        public LocationFormat()
        { }

        public LocationFormat(double latitude,  double longitude)
        {
            _coord.ConvertCoord(latitude, longitude);
        }

        public LocationFormat(double latitude, double longitude, FormatCoord formatView )
        {
            FormatView = formatView;

            _coord.ConvertCoord(latitude, longitude);

            UpdateCoordinatePos();

        }

       
        private void UpdateCoordinatePos()
        {
            
            switch (FormatView)
            {
                case FormatCoord.DD:
                    LatitudePos = _coord.coordDeg.Latitude.StringView;
                    LongitudePos = _coord.coordDeg.Longitude.StringView;
                    break;

                case FormatCoord.DD_MM_mm:
                    LatitudePos = _coord.coordDegMin.Latitude.StringView;
                    LongitudePos = _coord.coordDegMin.Longitude.StringView;
                    break;

                case FormatCoord.DD_MM_SS_ss:
                    LatitudePos = _coord.coordDegMinSec.Latitude.StringView;
                    LongitudePos = _coord.coordDegMinSec.Longitude.StringView;
                    break;

                default:
                    break;

            }
            
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
    }
}
