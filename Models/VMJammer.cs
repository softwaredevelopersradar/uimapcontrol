﻿using GrozaSModelsDBLib;
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UIMap.Events;
using UIMap.Models;
using WpfMapControl;

namespace UIMap
{
    public class VMJammer:TableJammerStation, INotifyPropertyChanged
    {
        public event EventHandler<EventArgs> OnUpdateBearing;       
        public event EventHandler<string> OnUpdateZone;

   

        private VMZone _zoneAlarm ;


        
        public VMZone ZoneAlarm 
        {
            get { return _zoneAlarm; }
            set
            {
                if (_zoneAlarm != value)
                {
                    _zoneAlarm = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneReadiness;

        public VMZone ZoneReadiness
        {
            get { return _zoneReadiness; }
            set
            {
                if (_zoneReadiness != value)
                {
                    _zoneReadiness = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneAttention;

        public VMZone ZoneAttention
        {
            get { return _zoneAttention; }
            set
            {
                if (_zoneAttention != value)
                {
                    _zoneAttention = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneJamming;

        public VMZone ZoneJamming
        {
            get { return _zoneJamming; }
            set
            {
                if (_zoneJamming != value)
                {
                    _zoneJamming = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneConnect;

        public VMZone ZoneConnect
        {
            get { return _zoneConnect; }
            set
            {
                if (_zoneConnect != value)
                {
                    _zoneConnect = value;
                    OnPropertyChanged();

                }
            }
        }



        private VMZone _zoneJammingSecond;

        public VMZone ZoneJammingSecond
        {
            get { return _zoneJammingSecond; }
            set
            {
                if (_zoneJammingSecond != value)
                {
                    _zoneJammingSecond = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneConnection;

        public VMZone ZoneConnection
        {
            get { return _zoneConnection; }
            set
            {
                if (_zoneConnection != value)
                {
                    _zoneConnection = value;
                    OnPropertyChanged();

                }
            }
        }



        private VMZone _zoneConnectionSecond;

        public VMZone ZoneConnectionSecond
        {
            get { return _zoneConnectionSecond; }
            set
            {
                if (_zoneConnectionSecond != value)
                {
                    _zoneConnectionSecond = value;
                    OnPropertyChanged();

                }
            }
        }



        private VMZone _zoneBearing;

        public VMZone ZoneBearing
        {
            get { return _zoneBearing; }
            set
            {
                if (_zoneBearing != value)
                {
                    _zoneBearing = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneOptic;

        public VMZone ZoneOptic
        {
            get { return _zoneOptic; }
            set
            {
                if (_zoneOptic != value)
                {
                    _zoneOptic = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneRotate;

        public VMZone ZoneRotate
        {
            get { return _zoneRotate; }
            set
            {
                if (_zoneRotate != value)
                {
                    _zoneRotate = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneRotateJamming;
        public VMZone ZoneRotateJamming
        {
            get { return _zoneRotateJamming; }
            set
            {
                if (_zoneRotateJamming != value)
                {
                    _zoneRotateJamming = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneRotateJammingSecond;
        public VMZone ZoneRotateJammingSecond
        {
            get { return _zoneRotateJammingSecond; }
            set
            {
                if (_zoneRotateJammingSecond != value)
                {
                    _zoneRotateJammingSecond = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneRotateConnection;
        public VMZone ZoneRotateConnection
        {
            get { return _zoneRotateConnection; }
            set
            {
                if (_zoneRotateConnection != value)
                {
                    _zoneRotateConnection = value;
                    OnPropertyChanged();

                }
            }
        }

        private VMZone _zoneRotateConnectionSecond;
        public VMZone ZoneRotateConnectionSecond
        {
            get { return _zoneRotateConnectionSecond; }
            set
            {
                if (_zoneRotateConnectionSecond != value)
                {
                    _zoneRotateConnectionSecond = value;
                    OnPropertyChanged();

                }
            }
        }


        private VMZone _zoneDirection;

        public VMZone ZoneDirection
        {
            get { return _zoneDirection; }
            set
            {
                if (_zoneDirection != value)
                {
                    _zoneDirection = value;
                    OnPropertyChanged();

                }
            }
        }


        private VMZone _zoneStaticJamming;

        public VMZone ZoneStaticJamming
        {
            get { return _zoneStaticJamming; }
            set
            {
                if (_zoneStaticJamming != value)
                {
                    _zoneStaticJamming = value;
                    OnPropertyChanged();

                }
            }
        }

        private EMapMode _regime = EMapMode.STOP;

        public EMapMode Regime
        {
            get { return _regime; }
            set
            {
                if (_regime != value)
                {
                    _regime = value;
                    OnPropertyChanged();
                    OnUpdateZone(this,"StaticJamming");

                }
            }
        }


        public ObservableCollection<VMBearing> BearingSource = new ObservableCollection<VMBearing>();



        public VMJammer() : base()
        {
            ZoneRotate = new VMZone("Rotate", this.Coordinates);
            ZoneOptic = new VMZone("Optic", this.Coordinates);
            ZoneBearing = new VMZone("Bearing", this.Coordinates);
            ZoneJamming = new VMZone("Jamming", this.Coordinates);
            ZoneJammingSecond = new VMZone("JammingSecond", this.Coordinates);
            ZoneConnection = new VMZone("Connection", this.Coordinates);
            ZoneConnectionSecond = new VMZone("ConnectionSecond", this.Coordinates);
            ZoneAttention = new VMZone("Attention", this.Coordinates);
            ZoneReadiness = new VMZone("Readiness", this.Coordinates);
            ZoneAlarm = new VMZone("Alarm", this.Coordinates);
            ZoneRotateJamming = new VMZone("RotateJamming", this.Coordinates);
            ZoneRotateJammingSecond = new VMZone("RotateJammingSecond", this.Coordinates);
            ZoneRotateConnection = new VMZone("RotateConnection", this.Coordinates);
            ZoneRotateConnectionSecond = new VMZone("RotateConnectionSecond", this.Coordinates);
            ZoneDirection = new VMZone("Direction", this.Coordinates);

            ZoneStaticJamming = new VMZone("StaticJamming", this.Coordinates);

            //BearingSource = new ObservableCollection<VMBearing>();
            BearingSource.CollectionChanged += BearingSource_CollectionChanged;

            ZoneRotate.OnUpdate += Zone_PropertyChanged;
            ZoneOptic.OnUpdate += Zone_PropertyChanged;
            ZoneBearing.OnUpdate += Zone_PropertyChanged;
            ZoneJamming.OnUpdate += Zone_PropertyChanged;
            ZoneJammingSecond.OnUpdate += Zone_PropertyChanged;
            ZoneConnection.OnUpdate += Zone_PropertyChanged;
            ZoneConnectionSecond.OnUpdate += Zone_PropertyChanged;
            ZoneAttention.OnUpdate += Zone_PropertyChanged;
            ZoneReadiness.OnUpdate += Zone_PropertyChanged;
            ZoneAlarm.OnUpdate += Zone_PropertyChanged;
            ZoneRotateJamming.OnUpdate += Zone_PropertyChanged;
            ZoneRotateJammingSecond.OnUpdate += Zone_PropertyChanged;
            ZoneRotateConnection.OnUpdate += Zone_PropertyChanged;
            ZoneRotateConnectionSecond.OnUpdate += Zone_PropertyChanged;
            ZoneDirection.OnUpdate += Zone_PropertyChanged;
            ZoneStaticJamming.OnUpdate += Zone_PropertyChanged;
        }



        public VMJammer(TableJammerStation obj) 
        {
            this.Id = obj.Id;
            this.Coordinates = obj.Coordinates;
           
            this.CallSign = obj.CallSign;
            this.DeltaTime = obj.DeltaTime;
            this.Note = obj.Note;
            this.Role = obj.Role;

            

            //BearingSource = new ObservableCollection<VMBearing>();
            BearingSource.CollectionChanged += BearingSource_CollectionChanged;

            

            ZoneRotate = new VMZone("Rotate", this.Coordinates);
            ZoneOptic = new VMZone("Optic", this.Coordinates);
            ZoneBearing = new VMZone("Bearing", this.Coordinates);
            ZoneJamming = new VMZone("Jamming", this.Coordinates);
            ZoneJammingSecond = new VMZone("JammingSecond", this.Coordinates);
            ZoneConnection = new VMZone("Connection", this.Coordinates);
            ZoneConnectionSecond = new VMZone("ConnectionSecond", this.Coordinates);
            ZoneAttention = new VMZone("Attention", this.Coordinates);
            ZoneReadiness = new VMZone("Readiness", this.Coordinates);
            ZoneAlarm = new VMZone("Alarm", this.Coordinates);
            ZoneRotateJamming = new VMZone("RotateJamming", this.Coordinates);
            ZoneRotateJammingSecond = new VMZone("RotateJammingSecond", this.Coordinates);
            ZoneRotateConnection = new VMZone("RotateConnection", this.Coordinates);
            ZoneRotateConnectionSecond = new VMZone("RotateConnectionSecond", this.Coordinates);
            ZoneDirection = new VMZone("Direction", this.Coordinates);
            ZoneStaticJamming = new VMZone("StaticJamming", this.Coordinates);



            ZoneRotate.OnUpdate += Zone_PropertyChanged;
            ZoneOptic.OnUpdate += Zone_PropertyChanged;
            ZoneBearing.OnUpdate += Zone_PropertyChanged;
            ZoneJamming.OnUpdate += Zone_PropertyChanged;
            ZoneJammingSecond.OnUpdate += Zone_PropertyChanged;
            ZoneConnection.OnUpdate += Zone_PropertyChanged;
            ZoneConnectionSecond.OnUpdate += Zone_PropertyChanged;
            ZoneAttention.OnUpdate += Zone_PropertyChanged;
            ZoneReadiness.OnUpdate += Zone_PropertyChanged;
            ZoneAlarm.OnUpdate += Zone_PropertyChanged;
            ZoneRotateJamming.OnUpdate += Zone_PropertyChanged;
            ZoneRotateJammingSecond.OnUpdate += Zone_PropertyChanged;
            ZoneRotateConnection.OnUpdate += Zone_PropertyChanged;
            ZoneRotateConnectionSecond.OnUpdate += Zone_PropertyChanged;
            ZoneDirection.OnUpdate += Zone_PropertyChanged;
            ZoneStaticJamming.OnUpdate += Zone_PropertyChanged;


        }

        private void Zone_PropertyChanged(object sender, EventArgs e)
        {
            VMZone temp = (VMZone)sender;
            
            OnUpdateZone?.Invoke(this, temp.Name);
        }

        public void RaiseBearingSourceCollectionChanged()
        {
            BearingSource_CollectionChanged(this, null);
        }

        private void BearingSource_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnUpdateBearing?.Invoke(this, new EventArgs());

        }

       
        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
