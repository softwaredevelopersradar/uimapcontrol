﻿using GrozaSModelsDBLib;
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap
{
    public class VMCuirasseTarget : INotifyPropertyChanged
    {
        Location locNull = new Location(-1, -1);

        private List<Location> _locationTrack = new List<Location>();

        public List<Location> LocationTrack
        {
            get => _locationTrack;
            set
            {                
                _locationTrack = value;
                OnPropertyChanged();
            }
        }


        private int _id;

        public int ID
        {
            get => _id;
            set
            {
                if (_id.Equals(value)) return;
                _id = value;

            }
        }

        

        public Mapsui.Styles.Pen PenTrack { get; set; }
   



        private Location _locationDrone;

        public Location LocationDrone
        {
            get => _locationDrone;
            set
            {
                if (_locationDrone.Equals(value)) return;
                _locationDrone = value;
                OnPropertyChanged();
            }
        }


        private double _frequency = -1;

        public double Frequency
        {
            get { return _frequency; }
            set
            {
                if (_frequency.Equals(value)) return;
                _frequency = value;

              
            }
        }


        private float _band = -1;

        public float Band
        {
            get { return _band; }
            set
            {
                if (_band.Equals(value)) return;
                _band = value;


            }
        }



        public VMCuirasseTarget() 
        {
            PenTrack =  new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
            PenTrack.PenStyle = Mapsui.Styles.PenStyle.Dot;
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
