﻿using Bearing;
using ClassLibraryPeleng;
using GrozaSModelsDBLib;
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public class VMBearing: INotifyPropertyChanged
    {
       
        public List<Location> locationBearing { get; private set; }


        private int _idSource;

        public int IDSource
        {
            get => _idSource;
            set
            {
                if (_idSource.Equals(value)) return;
                _idSource = value;
              
            }
        }




        private bool _isSelected;

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (_isSelected.Equals(value)) return;
                _isSelected = value;
                SetColorPen();
                OnPropertyChanged();
            }
        }



        private int _radius;

        public int Radius
        {
            get => _radius;
            set
            {
                if (_radius.Equals(value)) return;
                _radius = value;             
                UpdatePolyline();

            }
        }

        private Coord _center = new Coord();

        public Coord Center
        {
            get => _center;
            set
            {
                if (_center.Equals(value)) return;
                _center = value;
                UpdatePolyline();

            }
        }



        private float _direction;

        public float Direction
        {
            get => _direction;
            set
            {
                if (_direction.Equals(value)) return;
                _direction = value;
                UpdatePolyline();

            }
        }

        private bool _visible;

        public bool Visible
        {
            get => _visible;
            set
            {
                if (_visible.Equals(value)) return;
                _visible = value;
                UpdatePolyline();
            }
        }


        public Mapsui.Styles.Pen PenBearing { get; set; }

        private Mapsui.Styles.Pen PenBearingUsual { get; set; }
        private Mapsui.Styles.Pen PenBearingSelected { get; set; }


        public VMBearing(Coord center)
        {
            Center = center;
            PenBearingUsual = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
            PenBearingSelected = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Blue, 3);
            PenBearingSelected.PenStyle = Mapsui.Styles.PenStyle.LongDash;

            PenBearing = PenBearingUsual;
        }


        public VMBearing(Coord center, short radius, float sector, float direction, Mapsui.Styles.Pen penZone)
        {
            _center = center;
            _radius = radius;

            PenBearingUsual = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
            PenBearingSelected = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Blue, 1);

            PenBearing = PenBearingUsual;


            Direction = direction;
        }

        private void UpdatePolyline()
        {
            if (Radius > 0)
            {
                try
                {                    
                    var _step = 10000;
                    var _count_point = Radius / _step;
                    locationBearing = new List<Location>(_count_point);
                    locationBearing.Add(new Location(Center.Latitude, Center.Longitude));
                    for (int i = 1; i < _count_point; i++)
                    {
                        Coord temp = new Coord();
                        temp.Latitude = locationBearing.Last().Longitude;
                        temp.Longitude = locationBearing.Last().Latitude;

                        locationBearing.Add(DefinePointSquare(temp, Direction, _step));
                    }
                }

                catch { }
            }
            
        }

        private Location DefinePointSquare(Coord pointCenter, double Azimuth, double Distance)
        {
            Location location = new Location();

            try
            {

                double dLat = 0;
                double dLong = 0;
               
                ClassBearing.f_Bearing(Azimuth, Distance, 1, pointCenter.Latitude, pointCenter.Longitude, 1, ref dLat, ref dLong);

                location = new Location(dLat, dLong);
            }

            catch
            {

            }
            return location;
        }


        private void SetColorPen()
        {
            PenBearing = (IsSelected) ? PenBearingSelected : PenBearingUsual;
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
