﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public class VMSpoofingTrack : INotifyPropertyChanged
    {
        private List<Location> _locationTrack = new List<Location>();

        public List<Location> LocationTrack
        {
            get => _locationTrack;
            set
            {
                _locationTrack = value;
                OnPropertyChanged();
            }
        }



        private bool _visible = true;

        public bool Visible
        {
            get => _visible;
            set
            {
                if (_visible.Equals(value)) return;
                _visible = value;
                OnPropertyChanged();

            }
        }

        private bool _visibleTrack = false;

        public bool VisibleTrack
        {
            get => _visibleTrack;
            set
            {
                if (_visibleTrack.Equals(value)) return;
                _visibleTrack = value;
                OnPropertyChanged();

            }
        }

        public Mapsui.Styles.Pen PenTrack { get; set; }




        private Location _locationDrone;

        public Location LocationDrone
        {
            get => _locationDrone;
            set
            {
                if (_locationDrone.Equals(value)) return;
                _locationDrone = value;
                OnPropertyChanged();
            }
        }

        private float _angle;

        public float Angle
        {
            get => _angle;
            set
            {
                if (_angle.Equals(value)) return;
                _angle = value;
                OnPropertyChanged();
            }
        }

        private float _distance;

        public float Distance
        {
            get => _distance;
            set
            {
                if (_distance.Equals(value)) return;
                _distance = value;
                OnPropertyChanged();
            }
        }



        public VMSpoofingTrack()
        {
            PenTrack = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
            PenTrack.PenStyle = Mapsui.Styles.PenStyle.Dot;
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
