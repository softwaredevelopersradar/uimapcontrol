﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public class VMCuirassePoint
    {
        Location locNull = new Location(-1, -1);


        private int _id;

        public int ID
        {
            get => _id;
            set
            {
                if (_id.Equals(value)) return;
                _id = value;

            }
        }


        public Mapsui.Styles.Pen PenTrack { get; set; }


        private Location _locationPoint;

        public Location LocationPoint
        {
            get => _locationPoint;
            set
            {
                if (_locationPoint.Equals(value)) return;
                _locationPoint = value;
                OnPropertyChanged();
            }
        }


      
        private string _note = "";

        public string Note
        {
            get { return _note; }
            set
            {
                if (_note.Equals(value)) return;
                _note = value;


            }
        }


        public VMCuirassePoint()
        {
            PenTrack = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
            PenTrack.PenStyle = Mapsui.Styles.PenStyle.Dot;
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
