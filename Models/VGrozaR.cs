﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public class VGrozaR
    {
        public int ID { get; private set; }
        public IMapObject mapGrozaR { get; set; }

        public Feature mapZoneJamming { get; set; }

        public VGrozaR(int Id)
        {
            ID = Id;
            mapZoneJamming = new Feature();
        }
    }
}
