﻿using GrozaSModelsDBLib;
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap
{
    public class VMDrone: INotifyPropertyChanged
    {
        

        private List<Location> _locationTrack = new List<Location>();

        public List<Location> LocationTrack
        {
            get => _locationTrack;
            set
            {                
                _locationTrack = value;
                OnPropertyChanged();
            }
        }


        private int _idSource;

        public int IDSource
        {
            get => _idSource;
            set
            {
                if (_idSource.Equals(value)) return;
                _idSource = value;

            }
        }

        private string _name = "";

        public string Name
        {
            get => _name;
            set
            {
                if (_name.Equals(value)) return;
                _name = value;

            }
        }


        private bool _visible = true;

        public bool Visible
        {
            get => _visible;
            set
            {
                if (_visible.Equals(value)) return;
                _visible = value;
                OnPropertyChanged();

            }
        }

        private ViewUAV _view;

        public ViewUAV View
        {
            get => _view;
            set
            {
                if (_view.Equals(value)) return;
                _view = value;
                OnPropertyChanged();

            }
        }


        private bool _visibleTrack = false;

        public bool VisibleTrack
        {
            get => _visibleTrack;
            set
            {
                if (_visibleTrack.Equals(value)) return;
                _visibleTrack = value;
                OnPropertyChanged();

            }
        }

        public Mapsui.Styles.Pen PenTrack { get; set; }
   



        private Location _locationDrone;

        public Location LocationDrone
        {
            get => _locationDrone;
            set
            {
                if (_locationDrone.Equals(value)) return;
                _locationDrone = value;
                OnPropertyChanged();
            }
        }

        private float _distance;

        public float Distance
        {
            get => _distance;
            set
            {
                if (_distance.Equals(value)) return;
                _distance = value;
                OnPropertyChanged();
            }
        }


        private byte _zone;

        public byte Zone
        {
            get => _zone;
            set
            {
                if (_zone.Equals(value)) return;
                _zone = value;
                OnPropertyChanged();
            }
        }

        public VMDrone() 
        {
            PenTrack =  new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
            PenTrack.PenStyle = Mapsui.Styles.PenStyle.Dot;
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
