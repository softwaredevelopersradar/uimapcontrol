﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace UIMap.Models
{
    public class VMAeroscopeTarget : INotifyPropertyChanged
    {
        Location locNull = new Location(-1, -1);

        private List<Location> _locationTrack = new List<Location>();

        public List<Location> LocationTrack
        {
            get => _locationTrack;
            set
            {
                _locationTrack = value;
                OnPropertyChanged();
            }
        }


        private int _idSource;

        public int IDSource
        {
            get => _idSource;
            set
            {
                if (_idSource.Equals(value)) return;
                _idSource = value;

            }
        }

        private string _type = "";

        public string Type
        {
            get => _type;
            set
            {
                if (_type.Equals(value)) return;
                _type = value;

            }
        }


        private bool _visible = true;

        public bool Visible
        {
            get => _visible;
            set
            {
                if (_visible.Equals(value)) return;
                _visible = value;
                OnPropertyChanged();

            }
        }

        


        private bool _visibleTrack = false;

        public bool VisibleTrack
        {
            get => _visibleTrack;
            set
            {
                if (_visibleTrack.Equals(value)) return;
                _visibleTrack = value;
                OnPropertyChanged();

            }
        }

        public Mapsui.Styles.Pen PenTrack { get; set; }




        private Location _locationDrone;

        public Location LocationDrone
        {
            get => _locationDrone;
            set
            {
                if (_locationDrone.Equals(value)) return;
                _locationDrone = value;
                OnPropertyChanged();
            }
        }

        private Location _homePoint;

        public Location HomePoint
        {
            get => _homePoint;
            set
            {
                if (_homePoint.Equals(value)) return;
                _homePoint = value;
                OnPropertyChanged();
            }
        }

        public VMAeroscopeTarget()
        {
            PenTrack = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 255, 255, 0), 3);
            PenTrack.PenStyle = Mapsui.Styles.PenStyle.Dot;
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
