﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIMap.Models;

namespace UIMap.Events
{
    public class ZoneEventArgs
    {
        public VMZone _vmZone { get; private set; }

      

        public ZoneEventArgs(VMZone vmZone)
        {
            _vmZone = vmZone;
        }
    }
}
